
## About Test App
---

Test app has been created to get an understanding of Unit testing in the Laravel framework.

#### Prerequisites

- [PHP](https://www.php.net/): Version 7.3|8.0
- [Composer](https://getcomposer.org/)
- [Xdebug](https://xdebug.org/docs/install) Enabled in php.ini file. Used to generate code coverage reports.

#### Setup

Clone the git repository and go to the project directory and install the dependencies:

```
git clone
cd test-app
composer install
```

Create .env file if not exist:

```
cp env.example .env
php artisan key:generate
```
Craete the `test_app` database and configure it in .env file. After that run below command to migrate the database:

`php artisan migrate`

To server the application, run below command:

`php artisan serve`

To run the tests

**Windows**
`vendor\bin\phpunit`

**Mac & Linux**
`vendor/bin/phpunit`

We can also use Laravel command to run the tests `php artisan test`.

#### Code coverage

To generate the code coverage report, make sure you have enabled xdebux Extention on your machine. To verify that,

`php -v`

The above command should display the PHP details along with the xdebug. If all is good then you can generate the code coverage report using the below command,

`vendor\bin\phpunit --coverage-html reports/`


##### Code Coverage in PhpStorm

If you are a PhpStorm user, you can check code coverage result in phpstorm. For more information visit https://www.jetbrains.com/help/phpstorm/viewing-code-coverage-results.html

#### Note

For Windows users, If you have successfully installed xdebug but still receive error `No code coverage driver is available` while running tests then make sure below parameters are properly configured in `php.ini` file.

```
[xdebug]
zend_extension="c:/wamp64/bin/php/php7.4.9/zend_ext/php_xdebug-2.9.6-7.4-vc15-x86_64.dll"
xdebug.remote_enable = 1
xdebug.profiler_enable = on
xdebug.profiler_enable_trigger = on
xdebug.profiler_output_name = cachegrind.out.%t.%p
xdebug.profiler_output_dir ="c:/wamp64/tmp"
xdebug.show_local_vars=1
xdebug.mode=coverage,debug
xdebug.remote_handler = dbgp
xdebug.remote_host = localhost
xdebug.remote_autostart = 1
xdebug.remote_port = 9003
```

Linux and Mac users please visit [Configure Xdebug in PHP](https://xdebug.org/docs/install#configure-php)