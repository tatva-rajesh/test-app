<?php
namespace Tests\Unit;

use Tests\TestCase;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;

class UserTest extends TestCase
{
    use RefreshDatabase;

    public function testGetUsers() {
        $users = User::all();
        $this->assertCount(0, $users);
    }

    public function testCreateUser() {
        $user = User::factory()->make();
        $user->save();

        $users = User::count();
        $this->assertEquals(1, $users);
    }

    
}
