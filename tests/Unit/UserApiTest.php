<?php
namespace Tests\Unit;

use Tests\TestCase;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Facades\Artisan;

class UserApiTest extends TestCase
{
    use RefreshDatabase;

    public function testGetAllUser() {
        
        Artisan::call('db:seed');
        $usersAll = User::all()->toArray();

        $this->get('/users')
            ->assertStatus(200)
            ->assertExactJson(['success' => true, 'data' => $usersAll]);
    }

    public function testCreateNewUser() {

        // make a request without passing data, should get an error
        $this->postJson('/users')
            ->assertStatus(422)
            ->assertJsonValidationErrorFor('name')
            ->assertJsonValidationErrorFor('email');

        // pass invalid data
        $this->postJson('/users', ['email' => 'testuser', 'name' => 'Test User'])
            ->assertStatus(422)
            ->assertJsonValidationErrorFor('email');
        
        $user = User::factory()->make();
        
        $this->postJson('/users', [
            'name'  => $user->name,
            'email' => $user->email
        ])->assertStatus(201);

        $usersCount = User::where([
            'name' => $user->name,
            'email' => $user->email
        ])->count();

        $this->assertEquals(1, $usersCount);
    }

    public function testUpdateUser() {
        $user = User::factory()->make();
        $user->save();

        $data = [
            'name'  => "Test User One",
            'email' => "testuser1@gmail.com"
        ];

        $this->putJson("/users/" . $user->id, $data)->assertStatus(200);

        $user->refresh();

        $this->assertEquals($data['name'], $user->name);
        $this->assertEquals($data['email'], $user->email);

    }

    public function testDeleteUser() {
        $user = User::factory()->make();
        $user->save();

        // TODO:: Test api call and assert, use deleteJson method
    }
}
