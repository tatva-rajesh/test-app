<?php

namespace Tests\Unit;

use PHPUnit\Framework\TestCase;

class ExampleTest extends TestCase
{
    private $data = ['user1','user2', 'user3'];
    
    public function testArrayLength()
    {
        $result = count($this->data);

        $this->assertEquals(3, $result);
    }

    public function testAddElement() {
        array_push($this->data, 'user4');
        $this->assertContains('user4', $this->data);
    }
}
