<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;

class UserController extends Controller
{
    public function index()
    {
        $users = User::all();
        return response(['success' => true, 'data' => $users], 200);
    }

    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required|max:32',
            'email' => 'required|email|unique:users,email',
        ]);

        User::create(
            [
                'name' => $request->name,
                'email' => $request->email
            ]
        );

        return response(['success' => true], 201);
    }

    public function update(Request $request)
    {
        $user = User::find($request->user_id);

        if (!$user) {
            return response(['success' => false, 'error' => 'User not found!'], 200);
        }

        $request->validate([
            'name' => 'required|max:32',
            'email' => 'required|string|email|unique:users,email,' . $user->id,
        ]);

        $user->name  = $request->name;
        $user->email = $request->email;
        $user->save();

        return response(['success' => true], 200);
    }

    public function delete(Request $request)
    {
        $user = User::find($request->user_id);
        if (!$user) {
            return response(['success' => false, 'error' => 'User not found!'], 200);
        }

        $user->delete();

        return response(['success' => true], 200);
    }
}
